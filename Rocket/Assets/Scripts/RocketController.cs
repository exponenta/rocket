﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Collections;

public class RocketController : MonoBehaviour
{
	public float VibrateDurationGameEnd = 2f;
	public float VibrateDurationDamage = 0.25f;
	public float AcceleratorMultiplier = 1.5f;
	public float PowerOffDuration = 2f;
	public float MaxBoostVelocity = 10f;

	public float forceDelta = 15f;
	public float maxRotSpeed = 4f;
	public AudioSource EffectsSource;
	public AudioClip explosion;

	//[System.Serializable]
	public RocketConfig rocketConfig = new RocketConfig ();

	[HideInInspector]
	public	bool isRunned = false;
	private bool isFuelLow = false;
	private bool isContolled = true;

	[HideInInspector]
	public float hp = 1f;
	[HideInInspector]
	public float fuel = 1f;
	[HideInInspector]
	public Rigidbody2D _rigid;

	//[SerializeField]

	private float direction = 0;
	private Vector3 CalibrationVector = Vector3.zero;

	private Transform _particles;
	private Transform _boom;
	private Transform _max;
	private Transform _cofm;
	private AudioSource _sourse;

	float Logarifm (float f)
	{
		return Mathf.Log10 (1 + f * 10f);
	}

	float exp5 (float f)
	{
		return  Mathf.Exp (-3f * f);
	}

	float Sigmoid5 (float f)
	{
		return 2f / (1 + Mathf.Exp (-5f * f)) - 1f; 
	}


	float _mathFuelDecrement = 0;
	float _mathForceMultiplier = 0;

	void Start ()
	{
		Global.rocketConfig = rocketConfig;
		rocketConfig.LoadConfig ();

		_rigid = GetComponent<Rigidbody2D> ();
		_particles = transform.FindChild ("Particles");
		_boom = transform.FindChild ("Boom");
		_cofm = transform.FindChild ("CofM");
		_max = GameObject.Find ("Max").transform;

		_rigid.centerOfMass = _cofm.localPosition;

		_mathFuelDecrement = (0.1f + 0.9f * exp5 (rocketConfig.fuelDecrement)) * 0.15f;
		_mathForceMultiplier = Sigmoid5 (rocketConfig.forceMultiplier);

		Debug.Log ("Force:" + _mathForceMultiplier);
		Debug.Log ("Fuel:" + _mathFuelDecrement);

		_sourse = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	int ticks = 10;
	float accum = 0;
	float oldAccum = 0;

	void Update ()
	{

		//_mathFuelDecrement = (0.08f + 0.92f * exp5 (rocketConfig.fuelDecrement) ) * 0.25f;

		//_mathForceMultiplier = Sigmoid5 (rocketConfig.forceMultiplier);

		if (isRunned){
			if (Global.useTilt && isContolled) {
				ticks -= 1;
				accum += (Input.acceleration - CalibrationVector).x;
				if (ticks < 1) {
					ticks = 10;
					direction = (accum - oldAccum) * 0.1f * AcceleratorMultiplier;
					oldAccum = accum;
					accum = 0;
				}

			}

			if (_sourse.isPlaying) {
				float pitch = _rigid.velocity.magnitude * 0.05f;
				_sourse.pitch = pitch;
			}
		}
	}

	float timed = 0;


	void FixedUpdate ()
	{
		if (isRunned) {
			float vel = _rigid.velocity.magnitude;

			timed += Time.fixedDeltaTime;
			if (isContolled) {

				fuel -= _mathFuelDecrement * Time.fixedDeltaTime;

				float boost = 1 + Mathf.Clamp01 ((MaxBoostVelocity - vel) / MaxBoostVelocity) * 5; 
				Vector3 force_vector = transform.up * (_mathForceMultiplier * forceDelta * boost  + 100f);
				_rigid.AddForceAtPosition (force_vector, _cofm.position);

				if (fuel < 0.01f) {
					StartCoroutine (FuelLowTimer ());
				}

			}

			if (Mathf.Abs (transform.position.x) > _max.position.x) {

				RocketCrashedOrLost (Report.Lost);
			}
		}

		float airPres = (1f - rocketConfig.airDragRotate) * transform.right.y * _rigid.velocity.magnitude * 0.15f;
		_rigid.AddTorque (-direction * maxRotSpeed * rocketConfig.horizontalStability + airPres);
		_rigid.angularDrag = rocketConfig.horizontalStability * maxRotSpeed;
		_rigid.drag = 0.1f + rocketConfig.horizontalStability * Mathf.Abs (_rigid.velocity.normalized.x) * 0.6f;

	}

	IEnumerator FadeVolume(float s) {
		for (;;) {
			_sourse.volume = Mathf.Lerp (_sourse.volume, 0, Time.deltaTime * s);
			if (_sourse.volume < 0.03f) {
				_sourse.Stop ();
				AllreadyFade = false;
				break;
			}
			yield return null;
		}
	}

	private bool AllreadyFade = false;

	void DisableEffects() 
	{
		if (_sourse.isPlaying && !AllreadyFade) {
			AllreadyFade = true;
			StartCoroutine (FadeVolume(10f));
		}
		_particles.gameObject.SetActive(false);
	}

	void EnableEffects() {
		_sourse.Play ();
		_particles.gameObject.SetActive(true);
	}

	void ExplosionEffect(){
		_boom.gameObject.SetActive (true);
		if(Global.useMusic)
			EffectsSource.PlayOneShot (explosion);
	}

	void RocketCrashedOrLost (Report report)
	{
		if (!isRunned)
			return;
		
		isRunned = false;
		if (report == Report.Lost) {
			_rigid.velocity = Vector3.zero;
			_rigid.isKinematic = true;
		}

		DisableEffects ();

		Global.gameController.ShowStatus (report);
		StartCoroutine (GameEndWait (report));
	}

	IEnumerator FuelLowTimer ()
	{

		isFuelLow = true;
		isContolled = false;

		DisableEffects ();

		yield return new WaitForSeconds (3f);
		if (isRunned && fuel < 0.01f) {
			isRunned = false;
			Global.gameController.GameEnd (Report.FuelLow);
		}			
	}

	IEnumerator VibrateTime (float time)
	{
		var t = time;
		while (t > 0 && Global.useVibration) {
			Handheld.Vibrate ();
			t -= Time.deltaTime;
			//Debug.Log("[GAME] VIBRATE");
			yield return null;
		}
	}

	IEnumerator GameEndWait (Report report)
	{
		yield return new WaitForSeconds (2.0f);
		Global.gameController.GameEnd (report);
	}

	IEnumerator PowerOff ()
	{
		isContolled = false;

		DisableEffects ();

		yield return new WaitForSeconds (PowerOffDuration);

		if (hp > 0.01f && fuel > 0.01f) {
			//isFalling = false;
			isContolled = true;
			EnableEffects ();
		}
	}


	// Чет глюк со столкновением. Исправить
	public void OnCollisionEnter2D (Collision2D collision)
	{
		if (timed < 0.2f)
			return;
		//Debug.Log ("[GAME] TOUCHED");
		Collider2D coll = collision.collider;
		if (coll.tag == "Damaged" && isRunned) {

			if ((coll.name == "Ground" || coll.name == "Desk") && isRunned) {
				StartCoroutine (VibrateTime (VibrateDurationGameEnd));
				ExplosionEffect ();
				RocketCrashedOrLost (Report.Crash);
				_rigid.velocity = Vector3.zero;
			}
            
		}

	}

	public void OnTriggerEnter2D (Collider2D collider)
	{
		ObjectController oc = collider.GetComponentInParent<ObjectController> ();

		if (oc != null) {
			switch (oc.Type) {
			case ObjectController.ObjectType.Fuel:
				FuelUp (.7f);
				break;
			case ObjectController.ObjectType.Repair:
				hp = Mathf.Clamp01 (hp + 0.5f);
				break;
			case ObjectController.ObjectType.Electicity:
			case ObjectController.ObjectType.Damaged:
				AddDamage (oc);
				break;
			}
            
			oc.OnDestory ();
		}
	}

	void AddDamage (ObjectController oc)
	{
		float damage = oc.Damage;
		float pushForce = oc.pushForce;
		//Vector2 pushPos = (oc.transform.position + transform.position) * 0.5f;

		Vector2 pushNormal = -(oc.transform.position - transform.position).normalized; 
		StartCoroutine (VibrateTime (VibrateDurationGameEnd));

		hp -= damage;
		//античит
		if (pushNormal.y > 0.5f)
			pushNormal.y = -1f + Random.value * 1.5f;

		_rigid.AddForceAtPosition (pushNormal.normalized * pushForce, _cofm.position, ForceMode2D.Impulse);

		if (oc.Type == ObjectController.ObjectType.Electicity) {
			StartCoroutine (PowerOff ());
		}
         
		if (hp <= 0.01f) {
			_sourse.Stop ();
			StartCoroutine (VibrateTime (VibrateDurationGameEnd));

			ExplosionEffect ();

			RocketCrashedOrLost (Report.Crash);
		}
	}

	public void FuelUp (float _fuel)
	{

		if (hp > 0.01f) {
			fuel = Mathf.Clamp01 (_fuel + fuel);
		
			if (isFuelLow) {
				isContolled = true;
				isFuelLow = false;
				EnableEffects ();
			}

		}
	}


	public void Run ()
	{
		if (Global.useTilt) {
			CalibrationVector = Input.acceleration;
		}
		isRunned = true;
		isContolled = true;
		isFuelLow = true;
		hp = rocketConfig.maxHP;
		FuelUp (1f);
	}


	public void OnPointDown (BaseEventData e)
	{
		if (Global.useTilt && !isContolled)
			return;
		PointerEventData pe = (PointerEventData)e;
		direction = 3f * (pe.position.x / Screen.width - 0.5f);
	}

	public void OnPointUp (BaseEventData e)
	{
		if (Global.useTilt)
			return;
		//		PointerEventData pe = (PointerEventData)e;
		direction = 0;
	}
}
