﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UICustomSlider : MonoBehaviour {

	public int Minimal = 0;
	public int Maximum = 100;
	public int Step = 1;
	public int Value = 0;

	private Text _value;
	private Button _inc;
	private Button _dec;

	void Start () {
		_value = transform.FindChild ("Value").GetComponent<Text> ();
		_dec = transform.FindChild ("dec").GetComponent<Button> ();
		_inc = transform.FindChild ("inc").GetComponent<Button> ();

		_dec.onClick.AddListener (decValue);
		_inc.onClick.AddListener (incValue);

		Value = Mathf.Clamp (Value, Minimal, Maximum);
		_value.text = Value.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void incValue() {
		Value = Mathf.Clamp (Value + Step, Minimal, Maximum);
		_value.text = Value.ToString ();
	}

	public void decValue() {
		Value = Mathf.Clamp (Value - Step, Minimal, Maximum);
		_value.text = Value.ToString ();
	}
}
