﻿using UnityEngine;
using System.Collections;


public enum Report {Crash, Lost, FuelLow, Hard};
[System.Serializable]
public class CEvent: UnityEngine.Events.UnityEvent{};
[System.Serializable]
public class CEventReport: UnityEngine.Events.UnityEvent<Report>{};

public class GameController : MonoBehaviour {
	

	public GameObject statusLost;
	public GameObject statusCrash;

	public CEvent gameReplay;
	public CEvent gameRun;
	public CEventReport gameEnd;

	void Start () {
        //Global.LoadParams();
		Global.gameState = Global.State.Timer;
		Global.gameController = this;

		Global.musicCountainer.ChangeAudio ();
	}
	
	public void TimerEnd(){
		Global.gameState = Global.State.Runed;
		if (Global.musicCountainer != null) {
			Global.musicCountainer.ChangeAudio ();
		}
		gameRun.Invoke ();
	}

	public void PressReplay(){

		Global.gameState = Global.State.Timer;
        Global.restartsCount += 1;

        gameReplay.Invoke ();
		UnityEngine.SceneManagement.SceneManager.LoadScene ("game");
	}

	public void PressPause(){
		if (Global.musicCountainer != null) {
			Global.musicCountainer.ChangeAudio ();
		}

		Global.gameState = Global.State.Pause;
	}

    public void ResetReloadsCount()
    {
        Global.restartsCount = 0;
    }

	public void GameEnd(Report report) {
		Global.gameState = Global.State.End;

		if (Global.musicCountainer != null) {
			Global.musicCountainer.ChangeAudio ();
		}

        //сохраняем деньги и очки 
        Global.SaveParams();
        gameEnd.Invoke (report);
	}

	public void ShowStatus(Report report){
		if(report == Report.Lost)
			statusLost.SetActive(true);
		else
			statusCrash.SetActive (true);
	}

    public void ReturnToMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
}
