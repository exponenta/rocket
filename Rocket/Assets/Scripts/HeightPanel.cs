﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeightPanel : MonoBehaviour {


	public RectTransform point;
	public Text Value;
	public Transform Player;
	public Transform minPoint;
	public Transform maxPoint;
	[Space(2)]

	public float _value = 0;
	// Use this for initialization

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		_value = (Player.position.y - minPoint.position.y) /  (maxPoint.position.y - minPoint.position.y);
		point.anchoredPosition = new Vector2(_value * 480f, point.anchoredPosition.y);

		_value *= 240f * _value;
		if (_value < 1f) {
			_value *= 1000f;
			Value.text = _value.ToString ("##") + "m";
		} else {
			Value.text = _value.ToString ("##");
		}
	}
}
