﻿using UnityEngine;

using System.Collections;
using System;

[System.Serializable]
public class OtherResolution
{
    public int Height = 960;
    public float Size = 48f;
}

[ExecuteInEditMode]
public class PixelPerfect : MonoBehaviour
{


    public float textureSize = 100f;
    public int EditorHeight = 960;

    //public bool interpolate = true;
    public bool overideTextureSize = false;
    [SerializeField]
    public OtherResolution[] otherResolutions;
    float unitsPerPixel;
    // Use this for initialization

    int oldHeight = 0;
    
    private class Com : IComparer
    {
        public int Compare(object x, object y)
        {
            return ((x as OtherResolution).Height - (y as OtherResolution).Height);
        }
    }
    void Start()
    {
        Array.Sort(otherResolutions, new Com());
        Change();
    }

    void Change()
    {
        int height = Screen.height;

        if (Application.isEditor)
        {
            height = EditorHeight;
        }

        if (oldHeight != height)
        {

            oldHeight = height;
            float size = textureSize;

            if (overideTextureSize && otherResolutions.Length > 0)
            {

                for (int i = 0; i < otherResolutions.Length - 1; i++)
                {
                    var first = otherResolutions[i];
                    var second = otherResolutions[i + 1];

                    if (first.Height <= height && second.Height > height)
                    {
                        size = Mathf.Lerp(first.Size, second.Size, ((float)height - (float)first.Height) / ((float)second.Height - (float)first.Height));

                        break;
                    }
                }

                if (height < otherResolutions[0].Height)
                {
                    size = otherResolutions[0].Size;
                }
                if (height >= otherResolutions[otherResolutions.Length - 1].Height)
                {
                    size = otherResolutions[otherResolutions.Length - 1].Size;
                }
                //Debug.Log(size);
                
                //size = res.Size;
            }

            //unitsPerPixel = 1f / size;
            Camera.main.orthographicSize = height / (2f * Mathf.Round(size)) ;

        }
    }

    void Update()
    {
        Change();
    }
}