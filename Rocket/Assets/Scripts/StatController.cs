﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatController : MonoBehaviour {

	public Transform minPoint;
	public Transform maxPoint;
	[Space(10)]
	public RectTransform point;
	public Text HeightValue;
	[Space(10)]
	public Text ScoreValue;
	[Space(10)]
	public int monePerSecond = 2;
	[Space(10)]
	public GameObject StatsPanel;
	public Text StatScore;
	public Text StatMoney;
	public Text StatDamage;
	public Text StatHeight;
	public Text StatReport;
	[Space(10)]
	public Image fuelLine;
	public Image hpLine;
	public Text velText;

	private float _height = 0;
	private float _maxHeight = 0;
	private int _score = 0;
	private int _scoreBonus = 0;
	private float _money = 0;

	private RocketController _controller;
	void Start () {
        //Load saved money
        _money = (float) Global.MoneyCount;
		UpdateHeight ();
		UpdateScore ();
		_controller = GetComponent<RocketController> ();
	}

	// Update is called once per frame
	void Update (){
		
		if (Global.gameState == Global.State.Runed) {

			fuelLine.fillAmount = _controller.fuel;
			hpLine.fillAmount = _controller.hp / _controller.rocketConfig.maxHP;

			if (_controller.isRunned) {
				velText.text = string.Format ("{0:N1} m/s", _controller._rigid.velocity.y *_controller._rigid.velocity.y * 10f);
			} else {
				velText.text = "INF m/s";
			}

			_height = Mathf.Clamp01((transform.position.y - minPoint.position.y) /  (maxPoint.position.y - minPoint.position.y));
			_maxHeight = Mathf.Max (_height, _maxHeight);
			_score = Mathf.RoundToInt( _maxHeight * _maxHeight * 24000f) +  _scoreBonus;

			_money += Time.deltaTime * monePerSecond;

			UpdateHeight ();
			UpdateScore	();
		}
	}

	void UpdateScore() {
		ScoreValue.text = "Score:" + _score.ToString ("000000");
	}

	void UpdateHeight() {
		
		float _value = _height;
		point.anchoredPosition = new Vector2(_value * 480f, point.anchoredPosition.y);
		_value *= 240f * _value;

		if (_value < 1f) {
			_value *= 1000f;
			HeightValue.text = _value.ToString ("##") + "m";
		} else {
			HeightValue.text = _value.ToString ("##.##");
		}
	}
		
	public void ShowStats(Report report) {
        float realh = _maxHeight * _maxHeight * 240f;
		string h = realh.ToString ("####");
		if (realh < 1f)
			h = (realh * 1000f).ToString ("####") + " m";

        StatHeight.text = h;
		StatScore.text = Mathf.RoundToInt (_score).ToString();

        Global.MoneyCount = (int)_money;
        StatMoney.text = "$" + Global.MoneyCount;

		switch (report) {
		case Report.Crash:
			StatReport.text = "crash";
			break;
		case Report.Lost:
			StatReport.text = "lost";
			break;
		case Report.Hard:
			StatReport.text = "hard";
			break;
		case Report.FuelLow:
			StatReport.text = "fuel low";
			break;
		}
		StatsPanel.SetActive (true);
	}

	public void OnTriggerEnter2D (Collider2D collider)
	{
		switch (collider.tag) {
		case "Fuel":
			_scoreBonus += 100;
			Destroy (collider.gameObject);
			break;
		case "Coins":
			_scoreBonus += 50;
			_money += 50;

			Destroy (collider.gameObject);
			break;
		case "Damaged":
			_scoreBonus += 200;
			break;
		case "DamagedAndDestroed":
			_scoreBonus += 80;
			break;
		}

	}
}
