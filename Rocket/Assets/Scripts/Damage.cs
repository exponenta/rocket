﻿using UnityEngine;
using System.Collections;

public class Damage : MonoBehaviour {
	[Range(0,1.1f)]
	public float damage = 1f;
	public bool isDestroyed = false;

	public void onDestroy(){
		if (isDestroyed) {
			Destroy (gameObject);
		}
	}
}
