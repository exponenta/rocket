﻿using UnityEngine;
using System.Collections;

public class ObjectController : MonoBehaviour {

    public enum ObjectType { None, Fuel, Repair, Damaged, Electicity};

   // public bool PlayAnimatintWhenNear = true;
    public float Distance = 10f;
    public Transform Target;

    public bool Sleep = true;
    public bool Destoryed = true;
	public bool Passive = false;

    public ObjectType Type = ObjectType.None;

    public Transform baloon;
    public Transform child;

    public float movingUpSpeed = 10f;

    public ParticleSystem destroyParticles;

    [Range(0,1.1f)]
    public float Damage = 0.7f;
    public float pushForce = 70f;

    private bool _invisibleDestroy = false;
    private Animator _animator;
	private AudioSource _source;

    //private bool _off = true;

    void Start () {
        _animator = transform.GetComponentInChildren<Animator>();

        //if(PlayAnimatintWhenNear && _animator != null)
        //   _animator.enabled = false;
		if(_animator != null)
        	_animator.Play("idle", 0, Random.value);

        if (Target == null)
            Target = GameObject.Find("Rocket").transform;
		_source = GetComponent<AudioSource> ();
	}

    // Update is called once per frame
    void Update() {
		if (Passive)
			return;
		
        if (!Sleep)
        {

            if (Type != ObjectType.None && Type != ObjectType.Damaged && Type != ObjectType.Electicity)
            {
                transform.Translate(0, movingUpSpeed * Time.deltaTime, 0);
            }
        }


        if (!Sleep && _invisibleDestroy && Destoryed)
        {
            if ((transform.position - Target.position).sqrMagnitude >= Distance * Distance)
            {
                    gameObject.SetActive(false);
                    Destroy(gameObject);
            }
        }

    }
    
    public void OnDestory()
    {
		if (Passive)
			return;
		
        Sleep = false;
        if(Type != ObjectType.None && Type!= ObjectType.Damaged)
        {
            _invisibleDestroy = true;
            DestroyChild();

        } else
        {
            if (destroyParticles != null && Destoryed)
            {
                destroyParticles.gameObject.SetActive(true);

				var c = transform.GetComponentsInChildren<Collider2D> ();
				foreach (var item in c) {
					item.enabled = false;
				}

				if(_source)
					_source.PlayOneShot (_source.clip);
				Destroy(gameObject, Mathf.Max(destroyParticles.duration,  _source.clip.length));
            }
            
        }
    }
    


    void DestroyChild()
    {
        if (child && Destoryed)
        {
            child.gameObject.SetActive(false);
            Destroy(child.gameObject);
        }
        Sleep = false;
    }
}
