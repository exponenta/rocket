﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class ConfigControll : MonoBehaviour {
	
	public int OverideMoney = 10000;

    public Image lineEngine;
    public Image lineFuel;
    public Image lineAir;
    public Image lineStability;
	public Image lineHPRise;

    public Text bugete;
    public Text totalCost;

	public Text EngineCostText;
	public Text FuelCostText;
	public Text ConeCostText;
	public Text StabsCostText;

    public Button saveButton;

    [Space(10)]
    public int EngineUpdateCost = 350;
    public int FuelUpdateCost = 200;
    public int AirUpdateCost = 100;
    public int StabsUpdateCost = 280;

	public float UpdateMultiplier = 1.5f;

    private RocketConfig tmpConfig;
    private int _currentMoney = 0;

	private int _currentEngineCost(int s) {
			return 	(int)(EngineUpdateCost * Mathf.Pow (UpdateMultiplier, tmpConfig.EngineType + s -1));
	}

	private int _currentFuelCost(int s) { 
			return 	(int)(FuelUpdateCost * Mathf.Pow (UpdateMultiplier, tmpConfig.FuelType  + s - 1));
	}

	private int _currentConeCost(int s)  {
			return (int)(AirUpdateCost * Mathf.Pow (UpdateMultiplier, tmpConfig.ConeType + s -1));
	}

	private int _currentStabsCost (int s){
			return (int)(StabsUpdateCost * Mathf.Pow (UpdateMultiplier, tmpConfig.StabsType + s -1));
	}

    public void SetEngineParam(int sign)
    {
		if (sign > 0) {
			int cost = _currentEngineCost (0);
			if (_currentMoney >= cost && tmpConfig.EngineType < tmpConfig.CountOfUpdateTypes) {
				_currentMoney -= cost;
				tmpConfig.EngineType += 1;

				saveButton.interactable = true;
			}
		} else {
			int cost = _currentEngineCost (-1);
			if (tmpConfig.EngineType > Global.rocketConfig.EngineType) {
				_currentMoney += cost;
				tmpConfig.EngineType -= 1;

			}
		}
        UpdateConfig();
    }

    public void SetFuelParam(int sign)
    {

		if (sign > 0) {
			int cost = _currentFuelCost (0);
			if (_currentMoney >= cost && tmpConfig.FuelType < tmpConfig.CountOfUpdateTypes) {
				_currentMoney -= cost;
				tmpConfig.FuelType += 1;

				saveButton.interactable = true;
			}
		} else {
			int cost =  _currentFuelCost (-1);
			if (tmpConfig.FuelType > Global.rocketConfig.FuelType) {
				_currentMoney += cost;
				tmpConfig.FuelType -= 1;
			}
		}
		UpdateConfig();
    }

    public void SetAirParam(int sign)
    {

		if (sign > 0) {
			int cost = _currentConeCost(0);
			if (_currentMoney >= cost && tmpConfig.ConeType < tmpConfig.CountOfUpdateTypes) {
				_currentMoney -= cost;
				tmpConfig.ConeType += 1;

				saveButton.interactable = true;
			}
		} else {
			int cost =  _currentConeCost (-1);
			if (tmpConfig.ConeType> Global.rocketConfig.ConeType) {
				_currentMoney += cost;
				tmpConfig.ConeType -= 1;
			}
		}
		UpdateConfig();
    }

    public void SetStabsParam(int sign)
    {

		if (sign > 0) {
			int cost = _currentStabsCost(0);
			if (_currentMoney >= cost && tmpConfig.StabsType < tmpConfig.CountOfUpdateTypes) {
				_currentMoney -= cost;
				tmpConfig.StabsType += 1;

				saveButton.interactable = true;
			}
		} else {
			int cost =  _currentStabsCost (-1);
			if (tmpConfig.StabsType> Global.rocketConfig.StabsType) {
				_currentMoney += cost;
				tmpConfig.StabsType -= 1;
			}
		}
		UpdateConfig();
    }

	int  OldMoney = 0;
    public void SaveConfig()
    {

		Global.rocketConfig = tmpConfig.Clone();
        
		if (Global.CheaterMode)
			Global.MoneyCount = OldMoney;
		else
			Global.MoneyCount = _currentMoney;
		
        UpdateConfig();
        //save money
        Global.SaveParams();
    }

    public void LoadConfig()
    {
		tmpConfig = Global.rocketConfig.Clone();

		if (Global.CheaterMode) {
			OldMoney = Global.MoneyCount;
			Global.MoneyCount = OverideMoney;
		}

		_currentMoney = Global.MoneyCount;
        UpdateConfig();
    }

    public void UpdateConfig()
    {
		if (tmpConfig.EngineType != tmpConfig.CountOfUpdateTypes) {
			EngineCostText.text = "+ <color=maroon>$" + _currentEngineCost(0) + "</color>";
		} else {
			EngineCostText.text = "sales";
		}

		if (tmpConfig.FuelType != tmpConfig.CountOfUpdateTypes) {
			FuelCostText.text = "+ <color=maroon>$" + _currentFuelCost(0) + "</color>";
		} else {
			FuelCostText.text = "sales";
		}
		if (tmpConfig.ConeType != tmpConfig.CountOfUpdateTypes) {
			ConeCostText.text = "+ <color=maroon>$" + _currentConeCost(0) + "</color>";
		} else {
			ConeCostText.text = "sales";
		}
		if (tmpConfig.StabsType != tmpConfig.CountOfUpdateTypes) {
			StabsCostText.text = "+ <color=maroon>$" + _currentStabsCost(0) + "</color>";
		} else {
			StabsCostText.text = "sales";
		}

        lineEngine.fillAmount = tmpConfig.forceMultiplier;
        lineFuel.fillAmount = tmpConfig.fuelDecrement;
        lineAir.fillAmount = tmpConfig.airDragRotate;
        lineStability.fillAmount = tmpConfig.horizontalStability;

		lineHPRise.fillAmount = tmpConfig.maxHP - 1f;
        bugete.text = "$" + Global.MoneyCount;
        totalCost.text = "-$" + (Global.MoneyCount - _currentMoney);
    }

    public void Plus30ADS()
    {
        Global.adsController.ShowADS(delegate (UnityEngine.Advertisements.ShowResult r){
            if (r == UnityEngine.Advertisements.ShowResult.Finished) {
                Global.MoneyCount += 60;
                _currentMoney += 60;
                UpdateConfig();
            }/*
			if (r == UnityEngine.Advertisements.ShowResult.Skipped) {
				Global.MoneyCount += 30;
				_currentMoney += 30;
				UpdateConfig();
			}
            Debug.Log(r.ToString());*/
        }, true);
    }
}
