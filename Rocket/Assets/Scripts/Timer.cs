﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {
	
	
	public void TimeEnd() {
		gameObject.SetActive (false);
		Camera.main.GetComponent<GameController> ().TimerEnd ();
	}
}
