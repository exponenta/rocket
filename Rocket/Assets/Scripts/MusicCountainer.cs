﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class MusicCountainer : MonoBehaviour {

	private static MusicCountainer instance = null;

	public static MusicCountainer Instance {
	
		get { return instance; }

	}
	public float TransistDuration = 1f;
	public AudioMixerSnapshot Menu;
	public AudioMixerSnapshot Game;

	public AudioSource MenuSource;
	public AudioSource GameSource;

	void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
		Global.musicCountainer = this;
	}

	void Start() {
		Global.inMenu = true;
		ChangeAudio ();
	}


	IEnumerator StopGameMusic(float t) {
		yield return new WaitForSeconds (t);
		GameSource.Stop ();
	}

	public void StopOrStartAll(bool ins) {
		if (!ins) {
			GameSource.Stop ();
			MenuSource.Stop ();
		} else {
			ChangeAudio ();
		}
	}

	public void ChangeAudio() {
		if (!Global.useMusic) {
			StopOrStartAll(false);
			return;
		}
		switch (Global.gameState) {
		case Global.State.MainMenu:
		case Global.State.Pause:
		case Global.State.End:
			Menu.TransitionTo (TransistDuration);
			if (!MenuSource.isPlaying)
				MenuSource.Play ();
			StartCoroutine (StopGameMusic (TransistDuration));
			break;
		case Global.State.Timer:
		case Global.State.Runed:
			Game.TransitionTo (TransistDuration);
			if (!GameSource.isPlaying)
				GameSource.Play ();
			break;
		default:
			Menu.TransitionTo (TransistDuration);
			if (!MenuSource.isPlaying)
				MenuSource.Play ();
			break;
		}
	}
}
