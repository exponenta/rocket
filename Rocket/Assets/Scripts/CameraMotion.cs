﻿using UnityEngine;

public class CameraMotion : MonoBehaviour {
    
    public bool follow = true;
    public Transform player;
	public float yOffset;
	public float Shake = 0.6f;
	public Collider2D cameraBounds;
    
	public float speed = 10f;
    private Camera _camera;
	private Rigidbody2D _rigidbody;

	void Start () {
        _camera = GetComponent<Camera>();
		_rigidbody = player.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if(follow) {
            float aspect = _camera.pixelWidth * 1.0f / _camera.pixelHeight;
            
            
			float camsize = _camera.orthographicSize;
            
			Vector3 shake = new Vector3(Random.Range(-Shake, Shake), Random.Range(-Shake, Shake), 0) * _rigidbody.velocity.magnitude * 0.1f;
			Vector3 pos = player.position + shake;
			pos.y += camsize * yOffset;

			pos.x = Mathf.Clamp(pos.x, cameraBounds.bounds.min.x + aspect * camsize, cameraBounds.bounds.max.x - aspect * camsize);
            pos.y = Mathf.Clamp(pos.y, cameraBounds.bounds.min.y + camsize, cameraBounds.bounds.max.y - camsize);
            pos.z = transform.position.z;
            
            transform.position = pos;
            
        }
        
	}
}
