﻿using UnityEngine;
using System.Collections;

public class StarsBlink : MonoBehaviour {

	public float minDT = 2f;
	public float maxDT = 5f;

	public Sprite[] sprites;

	private int index = 0;
	private float dt = 0;

	private SpriteRenderer _renderer;
	private bool _update = true;

	IEnumerator StarBlink() {

		yield return new WaitForSeconds (dt);
		index = Mathf.RoundToInt (Random.value * sprites.Length) % sprites.Length;
		_renderer.sprite = sprites [index];
		if(_update)
			StartCoroutine (StarBlink ());
	}

	void Start () {

		dt = minDT + Random.value* (maxDT - minDT);
		_renderer = GetComponent<SpriteRenderer> ();
		index = Mathf.RoundToInt (Random.value * sprites.Length) % sprites.Length;
		_renderer.sprite = sprites [index];

		StartCoroutine (StarBlink());
	}

	void OnBecameVisible() {
		if(!_update)
			StartCoroutine (StarBlink ());
		_update = true;

	}

	void OnBecameInvisible() {
		_update = false;
	}

}
