﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {

    public Toggle Tilt;
    public Toggle Music;
    public Toggle Vibration;
	public Text About;

    void Start()
    {
        Global.LoadParams();
		Tilt.interactable = false;
		Music.interactable = false;
		Vibration.interactable = false;

        Tilt.isOn = Global.useTilt;
        Music.isOn = Global.useMusic;
        Vibration.isOn = Global.useVibration;

		Tilt.interactable = true;
		Music.interactable = true;
		Vibration.interactable = true;

		string ver = Application.version;
		string cheater = Global.CheaterMode?"=)))":"";
		About.text = About.text.Replace ("#ver#", ver);
		About.text = About.text.Replace ("#ch#", cheater);

		Debug.Log ("CheateMode:" + Global.CheaterMode);
		Global.gameState = Global.State.MainMenu;
		Global.musicCountainer.ChangeAudio ();
    }

	void Update() {
		if (Input.GetKeyUp (KeyCode.Escape)) {
			Application.Quit ();
		}	
	}

    public void ToSceene(string name) {
		UnityEngine.SceneManagement.SceneManager.LoadScene(name);
	}

	public void SetTilt(bool on) {
		Global.useTilt = on;
	}
	public void SetMusic(bool on) {
		Global.useMusic = on;
	}
	public void SetVibration(bool on) {
		Global.useVibration = on;
	}

    public void SetConfig()
    {
        //Global.useTilt = Tilt.isOn;
       // Global.useMusic = Music.isOn;
        //Global.useVibration = Vibration.isOn;
        Global.SaveOnlySettings();
    }

	int clicks = 0;

	public void EnableCheater() {
		clicks++;
		if (clicks > 5 && !Global.CheaterMode) {
			Global.CheaterMode = true;
			Global.SaveOnlySettings ();
			Debug.Log ("Cheate mode ON");
			About.text += "=)))";
		}

	}

	public void setMusicState(bool ins) {
		Global.musicCountainer.StopOrStartAll (ins);
	}

    public void ResetStatistic()
    {
        Global.ResetStatistic();
		Debug.Log ("CheateMode:" + Global.CheaterMode);
		About.text = About.text.Replace ("=)))", "");
    }
}
