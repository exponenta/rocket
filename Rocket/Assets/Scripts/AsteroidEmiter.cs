﻿using UnityEngine;
using System.Collections;

public class AsteroidEmiter : MonoBehaviour {

	public Transform[] Asteroids;
	public float minEmitDuration = 2f;
	public float maxEmitDuration = 10f;
	public float EmitRadius = 4f;
	public float MinEmitAngle = 30f;
	public float MaxEmitAngle = 80f;

	public float MinEmitDirectonAngle = 30f;
	public float MaxEmitDirectionAngle = 80f;

	private Transform _player;
	private bool _playerEnter = false;

	void Start () {

		_player = GameObject.Find ("Rocket").transform;

	}


	IEnumerator EmitCourutine() {
		Emit ();
		float dur = Random.Range (minEmitDuration, maxEmitDuration);
		yield return new WaitForSeconds (dur);
		if (_playerEnter == true) {
			StartCoroutine (EmitCourutine ());
		}
	}

	void Emit() {

		int ind = Random.Range (0, Asteroids.Length);
		Transform inst = Instantiate (Asteroids [ind]) as Transform;

		float _a = MinEmitAngle + (MaxEmitAngle - MinEmitAngle) * Random.value;

		var _direction = new Vector3 (Mathf.Cos (Mathf.Deg2Rad * _a) * EmitRadius, Mathf.Sin (Mathf.Deg2Rad * _a) * EmitRadius, 0);
		var _pos = _player.position + _direction;

		inst.position = _pos;
		inst.gameObject.SetActive (true);
		inst.SetParent (transform);
		var dr = Random.Range (MinEmitDirectonAngle, MaxEmitDirectionAngle);
		inst.GetComponent<AsteroidMotor> ().moveDirection = -new Vector3 (Mathf.Cos (Mathf.Deg2Rad * dr) , Mathf.Sin (Mathf.Deg2Rad * dr), 0);;

	}

	void OnTriggerEnter2D(Collider2D col) {
		Debug.Log (col);
		if (col.tag == "Player") {
			_playerEnter = true;
			StartCoroutine (EmitCourutine ());
		}
	}

	void OnTriggerExit2D(Collider2D col) {
		if (col.tag == "Player") {
			_playerEnter =false;
		}
	}
}
