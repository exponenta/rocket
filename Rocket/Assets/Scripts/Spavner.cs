﻿using UnityEngine;
using System.Collections;

public class Spavner : MonoBehaviour {

	public Transform[] Objs;
	public int count = 20;
	public float range = 0.4f;
	public float deltaZ = 0;
	public Collider2D areal;

	void Start () {
		if (Objs.Length > 0 && areal != null)
			Spavn ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Spavn(){
		
		int cx = Mathf.RoundToInt (Mathf.Sqrt (count));
		int cy = Mathf.RoundToInt (count / cx);
		float dx = areal.bounds.size.x / cx;
		float dy = areal.bounds.size.y / cy;

		for (int x = 0; x < cx ; x++) {
			for(int y = 0; y < cy; y++ ){
				
				Random.seed = System.DateTime.Now.Millisecond * (x+1) * (y+1);
				float _x = (x + 0.5f) * dx;
				float _y = (y + 0.5f) * dy;
				float val = 0.5f -Random.value;

				_x += areal.bounds.center.x - areal.bounds.size.x * 0.5f+ val * dx * range;
				_y += areal.bounds.center.y - areal.bounds.size.y * 0.5f+ (-val) * dy * range;

				int ri = Mathf.RoundToInt (Random.value * Objs.Length) % Objs.Length;

				Transform inst = Instantiate (Objs[ri]) as Transform;
				inst.position = new Vector3 (_x, _y, Objs[ri].position.z + deltaZ * Random.value);
				inst.SetParent (transform);
			}
		}
	}

}
