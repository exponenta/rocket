﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class RocketConfig
{
	public int version = 1;
	public float minValue = 0.1f;

	public int CountOfUpdateTypes = 5;

	public int ConeType = 1;
	public int FuelType = 1;
	public int EngineType = 1;
	public int StabsType = 1;

	public float forceMultiplier {
		get {
			return (EngineType) / (float)CountOfUpdateTypes;
		}	
	}

	public float airDragRotate {
		get {
			return (ConeType) / (float)CountOfUpdateTypes;
		}	
	}

	public float fuelDecrement {
		get {
			return (FuelType) / (float)CountOfUpdateTypes;
		}	
	}

	public float horizontalStability {
		get {
			return (StabsType) / (float)CountOfUpdateTypes;
		}	
	}

	public float maxHP {
		get {
			return 1f + (float)(ConeType + FuelType + EngineType + StabsType - 4) / 16f;
		}
	}


	public RocketConfig Clone() {
		RocketConfig rc = new RocketConfig ();
		rc.ConeType = ConeType;
		rc.EngineType = EngineType;
		rc.FuelType = FuelType;
		rc.StabsType = StabsType;
		return rc;
	}

	public void SaveConfig ()
	{
		PlayerPrefs.SetInt ("config_version", version);

		PlayerPrefs.SetInt ("rocket_coneType", ConeType);
		PlayerPrefs.SetInt ("rocket_fuelType", FuelType);
		PlayerPrefs.SetInt ("rocket_engineType", EngineType);
		PlayerPrefs.SetInt ("rocket_stabsType", StabsType);

		PlayerPrefs.Save ();
	}

	public void LoadConfig ()
	{
		int v = PlayerPrefs.GetInt ("config_version", 0);
		if (v == 0) {
			
			ConeType = Mathf.RoundToInt (PlayerPrefs.GetFloat ("rocket_airDragRotate", 0.2f) * CountOfUpdateTypes);
			FuelType = Mathf.RoundToInt (PlayerPrefs.GetFloat ("rocket_fuelDecrement", 0.2f) * CountOfUpdateTypes);
			EngineType = Mathf.RoundToInt (PlayerPrefs.GetFloat ("rocket_forceMultiplier", 0.2f) * CountOfUpdateTypes);
			StabsType = Mathf.RoundToInt (PlayerPrefs.GetFloat ("rocket_horizontalStability", 0.2f) * CountOfUpdateTypes);
			SaveConfig ();

		} else {

			ConeType = (int)PlayerPrefs.GetInt ("rocket_coneType", 1);
			FuelType = (int)PlayerPrefs.GetInt ("rocket_fuelType", 1);
			EngineType = (int)PlayerPrefs.GetInt ("rocket_engineType", 1);
			StabsType = (int)PlayerPrefs.GetInt ("rocket_stabsType", 1);
		}
	}
}

public static class Global
{
	public enum State
	{
		Pause,
		MainMenu,
		End,
		Runed,
		Timer,
		None
	}

	public static int Score = 0;
	public static bool useTilt = false;
	public static bool useMusic = true;
	public static bool useVibration = true;
	public static State gameState = State.None;
	public static RocketConfig rocketConfig = null;
	public static GameController gameController;
	public static ADS adsController;
	public static int restartsCount = 0;
	public static int MoneyCount = 0;
	public static bool CheaterMode = false;
	public static bool inMenu = true;
	public static MusicCountainer musicCountainer;

	public static void SaveParams ()
	{
		
		PlayerPrefs.SetInt ("game_money", MoneyCount);
		/*
		PlayerPrefs.SetInt ("settings_tilt", useTilt ? 1 : 0);
		PlayerPrefs.SetInt ("settings_music", Music ? 1 : 0);
		PlayerPrefs.SetInt ("settings_vibration", Vibration ? 1 : 0);*/
		SaveOnlySettings ();

		int s = PlayerPrefs.GetInt ("game_maxscore", 0);
		if (s < Score)
			PlayerPrefs.SetInt ("game_maxscore", Score);

		rocketConfig.SaveConfig ();

		Debug.Log ("[GLOBAL] SAVE ALL SETTINGS");
	}

	public static void SaveOnlySettings ()
	{
		Debug.Log ("[GLOBAL] SAVE CONFIG");
		PlayerPrefs.SetInt ("settings_cheater", CheaterMode? 1:0);
		PlayerPrefs.SetInt ("settings_tilt", useTilt ? 1 : 0);
		PlayerPrefs.SetInt ("settings_music", useMusic ? 1 : 0);
		PlayerPrefs.SetInt ("settings_vibration", useVibration ? 1 : 0);
		PlayerPrefs.Save ();
	}

	public static void LoadParams ()
	{
		CheaterMode = PlayerPrefs.GetInt ("settings_cheater", 0) == 1;
		MoneyCount = PlayerPrefs.GetInt ("game_money", 0);
		useTilt = PlayerPrefs.GetInt ("settings_tilt", 1) == 1;
		useMusic = PlayerPrefs.GetInt ("settings_music", 1) == 1;
		useVibration = PlayerPrefs.GetInt ("settings_vibration", 1) == 1;
		rocketConfig = new RocketConfig ();
		rocketConfig.LoadConfig ();
		Debug.Log ("[GLOBAL] LOAD SETTINGS");
	}

	public static void ResetStatistic ()
	{
		rocketConfig = new RocketConfig ();
		MoneyCount = 0;
		Score = 0;
		PlayerPrefs.SetInt ("game_maxscore", Score);
		CheaterMode = false;
		SaveParams ();
	}
}
