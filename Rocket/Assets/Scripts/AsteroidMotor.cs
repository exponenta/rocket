﻿using UnityEngine;
using System.Collections;

public class AsteroidMotor : MonoBehaviour {

	public bool DestroyOnIvisible = true;
	public float moveSpeed = 10f;
	public Vector3 moveDirection = Vector3.zero;
	public ParticleSystem particle;

	private bool _willVisible = false;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.up = -moveDirection;
		transform.Translate (moveDirection * moveSpeed * Time.deltaTime, Space.World);

	}

	void OnBecameVisible() {
		_willVisible = true;
	}

	void OnBecameInvisible() {
		if (_willVisible && DestroyOnIvisible) {
			gameObject.SetActive (false);
			Destroy (gameObject);
		}
	}


	void OnTriggerEnter2D(Collider2D col) {

		if (col.tag == "Player") {
			if (particle != null) {
				particle.gameObject.SetActive (true);
				//particle.Play ();

				DestroyOnIvisible = false;
				GetComponent<SpriteRenderer> ().enabled = false;
				GetComponent<BoxCollider2D> ().enabled = false;
				Destroy (gameObject, particle.duration);
			} else {
				Destroy (gameObject);
			}
		}
	}
}
