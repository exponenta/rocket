﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections.Generic;

public class ADS : MonoBehaviour
{

    public string androidID;
    public string iosID;
    public bool test = false;
    [Space(10)]
    public bool showAfterRestart = true;
    public int restartsCount = 5;

    void Awake()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Advertisement.Initialize(androidID, test);
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Advertisement.Initialize(iosID, test);
        }
      
    }
    
    public void Start()
    {
        Global.gameController.gameEnd.AddListener(ShowADSAfterEnd);
        Global.adsController = this;
    }

    public void ShowADSAfterEnd(Report r)
    {
        if(showAfterRestart && Global.restartsCount >= restartsCount)
        {
            ShowRewardADS();
            Global.restartsCount = 0;
        }
    }

    public void ShowADS(System.Action<ShowResult> callback, bool rewarded = true)
    {
        string type = rewarded ? "rewardedVideo" : "video";
        if (Advertisement.IsReady(type))
        {
            ShowOptions options = new ShowOptions {resultCallback = callback};
            Advertisement.Show(type, options);
        }
    }

    public void ShowRewardADS()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            ShowOptions options = new ShowOptions { resultCallback = ADSCallback };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    public void ShowADS()
    {
        if (Advertisement.IsReady("video"))
        {
            ShowOptions options = new ShowOptions { resultCallback = ADSCallback };
            Advertisement.Show("video", options);
        }
    }

    private void ADSCallback(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("[ADS] Finished");
                break;
            case ShowResult.Skipped:
                Debug.Log("[ADS] Skipped");
                break;
            case ShowResult.Failed:
                Debug.Log("[ADS] Failed");
                break;
        }
    }
}
